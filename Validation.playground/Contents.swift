


protocol Validator {
    func validateFormat(dataValidation: String) -> Bool
    func getMessageError() -> String
}

class IsNumber: Validator {
    func validateFormat(dataValidation: String) -> Bool {
        return Int(dataValidation) == nil ? false : true
    }
    func getMessageError() -> String {
        return "input ไม่ใช่ number นะเด็กๆ"
    }
}

class IsTenDigit: Validator {
    func validateFormat(dataValidation: String) -> Bool {
        return dataValidation.characters.count == 10 ? true : false
    }
    func getMessageError() -> String {
        return "input ไม่ใช่ 10 หลักนะหนูๆ"
    }
}






let isNumber = IsNumber()
isNumber.validateFormat(dataValidation: "123123123")
isNumber.validateFormat(dataValidation: "123123123aaa")



let isTenDigit = IsTenDigit()
isTenDigit.validateFormat(dataValidation: "12312312311")
isTenDigit.validateFormat(dataValidation: "1231231231")


//let testValidateString = "Piyawut"
//if !isNumber.validateFormat(dataValidation: testValidateString) {
//    print("is not number")
//}
//else if !isTenDigit.validateFormat(dataValidation: testValidateString){
//    print("is not ten digit")
//}else {
//    print("is number and ten digit")
//}



//let testValidateString = "Piyawut"
//var numberAndTenDigitRule: [Validator] = [isNumber , isTenDigit]
//numberAndTenDigitRule.forEach { (validator) in
//    if !validator.validateFormat(dataValidation: testValidateString) {
//        print("error someting")
//    }
//}






extension Array where Element == Validator {
    func runValidator(testString: String) -> String? {
        var stringError: String?
        self.forEach { (validator) in
            if !validator.validateFormat(dataValidation: testString) {
                stringError =  validator.getMessageError()
                return
            }
        }
        return stringError
    }
}


let testValidateString = "Piyawut"
var numberAndTenDigitRule: [Validator] = [isNumber , isTenDigit]
print(numberAndTenDigitRule.runValidator(testString: testValidateString))


